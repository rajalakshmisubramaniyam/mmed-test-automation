package com.mmed.codebase;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import com.mmed.utils.Utils;

import io.github.bonigarcia.wdm.WebDriverManager;

public class QABase {
	
	public static WebDriver driver;
	public static final long PAGE_LOAD_TIMEOUT = 60;
	public static final long IMPLICIT_WAIT = 20;
	public static final String CURRENT_DIRECTORY = System.getProperty("user.dir");
	
	public static void initialization() throws IOException {
		String browserName = Utils.getPropertyValue("browserName");
		String browserVersion = Utils.getPropertyValue("browserVersion");
		
		if(browserName.equals("chrome")){
			WebDriverManager.chromedriver().browserVersion(browserVersion).setup();
			ChromeOptions options = new ChromeOptions();
			driver = new ChromeDriver(options);
		}
		else if(browserName.equals("firefox")){
			WebDriverManager.firefoxdriver().browserVersion(browserVersion).setup();
			FirefoxOptions options = new FirefoxOptions();
			driver = new FirefoxDriver(options);
		}
		else if(browserName.equals("edge")){
			WebDriverManager.edgedriver().browserVersion(browserVersion).setup();
			EdgeOptions options = new EdgeOptions();
			driver = new EdgeDriver(options);
		}else if(browserName.equals("ie")){
			WebDriverManager.iedriver().browserVersion(browserVersion).setup();
			InternetExplorerOptions options = new InternetExplorerOptions();
			driver = new InternetExplorerDriver(options);
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS); 
		driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
	}	
	
	public static void takeScreenshot() throws Exception {
		String filePath = CURRENT_DIRECTORY + "\\" + Utils.getPropertyValue("outputData") + "\\" + "testing1.png";
		File s = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    FileUtils.copyFile(s, new File(filePath));
	}
	
	public static void takeScreenshotOfWebElement(WebElement element) throws Exception {
		  String filePath = CURRENT_DIRECTORY + "\\" + Utils.getPropertyValue("outputData") + "\\" + "testing2.png";
	      File f = element.getScreenshotAs(OutputType.FILE);
	      FileUtils.copyFile(f, new File(filePath));
	}
}
