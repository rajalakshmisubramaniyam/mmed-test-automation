package com.mmed.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.Platform;

public class ExcelUtils {
    
	private ExcelUtils() {
		
	}
	
	public static final String CURRENT_DIRECTORY = System.getProperty("user.dir");
    private static String testDataExcelPath = null;
	private static HSSFWorkbook excelWBook;
	private static HSSFSheet excelWSheet;
    private static HSSFCell cell;
    private static HSSFRow row;
    
	public static int rowNumber;
	public static int columnNumber;

    public static void setExcelFileSheet(String testDataExcelFileName, String sheetName) throws Exception {
    	if (Platform.getCurrent().toString().equalsIgnoreCase("MAC")) {
            testDataExcelPath = CURRENT_DIRECTORY + "/";
        } else if (Platform.getCurrent().toString().contains("WIN")) {
            testDataExcelPath = CURRENT_DIRECTORY + "\\";
        }
        FileInputStream ExcelFile = new FileInputStream(testDataExcelPath + "\\" + testDataExcelFileName);
        excelWBook = new HSSFWorkbook(ExcelFile);
        excelWSheet = excelWBook.getSheet(sheetName);
    }

    public static String getCellData(int RowNum, int ColNum) {
        cell = excelWSheet.getRow(RowNum).getCell(ColNum);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public static HSSFRow getRowData(int RowNum) {
        row = excelWSheet.getRow(RowNum);
        return row;
    }

    public static void setCellData(String testDataExcelFileName, int RowNum, int ColNum, String value) throws Exception {
        row = excelWSheet.getRow(RowNum);
        cell = row.getCell(ColNum);
        if (cell == null) {
            cell = row.createCell(ColNum);
            cell.setCellValue(value);
        } else {
            cell.setCellValue(value);
        }
        FileOutputStream fileOut = new FileOutputStream(testDataExcelPath + testDataExcelFileName);
        excelWBook.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }
}