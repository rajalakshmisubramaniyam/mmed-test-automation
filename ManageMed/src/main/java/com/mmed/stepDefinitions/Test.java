package com.mmed.stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.sql.Driver;

import org.openqa.selenium.By;

import com.mmed.codebase.QABase;
import com.mmed.pages.TestPage;
import com.mmed.utils.ExcelUtils;
import com.mmed.utils.Utils;

public class Test extends QABase{
	
	TestPage testPage = new TestPage();
	
	@Given("^Run sample test$")
	public void testRun() throws IOException  {
		driver.get(Utils.getPropertyValue("URL"));
		testPage.getInputField().sendKeys("My Testing");
	}
	
	@Given("^Read Excel File$")
	public void readExcel() throws Exception {
		ExcelUtils.setExcelFileSheet(Utils.getPropertyValue("inputTestData"), "Sheet1");
		ExcelUtils.getRowData(3);
		ExcelUtils.getCellData(3, 2);
	}
}


