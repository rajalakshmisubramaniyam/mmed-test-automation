package com.mmed.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mmed.codebase.QABase;

public class TestPage extends QABase {
	
	public TestPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input")
	private  WebElement inputField;	

    public WebElement getInputField() {
		return inputField;
    }
    
    public void setInputField(WebElement inputField) {
		this.inputField = inputField;
	}
}
